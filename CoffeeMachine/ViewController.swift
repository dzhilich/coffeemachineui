//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by Dmytro Zhylich on 24.04.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tanksInfo: UILabel!
    @IBOutlet weak var statusView: UILabel!
    
    
    let machine = CoffeeMachineUI()
    
    
    @IBAction func showTanksInfo(_ sender: UIButton) {
        tanksInfo.text = machine.showTankInfo()
    }
    
    // Add Btn Actions
    @IBAction func addMilkAction(_ sender: UIButton) {
        statusView.text = machine.addMilk()
    }
    @IBAction func addCoffeeAction(_ sender: UIButton) {
        statusView.text = machine.addCoffee()
    }
    @IBAction func addWaterAction(_ sender: UIButton) {
        statusView.text = machine.addWater()
    }
    @IBAction func clearTrash(_ sender: UIButton) {
        statusView.text = machine.clearTrash()
    }
    
    
    // make drinks
    @IBAction func makeAction(_ sender: UIButton) {
        var res = ""
        
        switch sender.currentTitle {
            case DrinkType.cappuccino.rawValue:
                res = machine.makeADrink(DrinkType.cappuccino)
            case DrinkType.americano.rawValue:
                res = machine.makeADrink(DrinkType.americano)
            case DrinkType.espresso.rawValue:
                res = machine.makeADrink(DrinkType.espresso)
            case DrinkType.hotMilk.rawValue:
                res = machine.makeADrink(DrinkType.hotMilk)
            default:
                res = "Not fount"
        }
        
        statusView.text = res
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
}
