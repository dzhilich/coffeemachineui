//
//  CoffeeMachine.swift
//  CoffeeMachine
//
//  Created by Dmytro Zhylich on 24.04.2021.
//

import Foundation


enum DrinkType: String {
    case cappuccino, espresso, americano, hotMilk

    var coffeeNeeded: Int {
        switch self {
        case .cappuccino: return 200
        case .americano: return 500
        case .espresso: return 1000
        case .hotMilk: return 0
        }
    }

    var waterNeeded: Int {
        switch self {
        case .cappuccino: return 50
        case .americano: return 500
        case .espresso: return 100
        case .hotMilk: return 0
        }
    }
    
    var milkNeeded: Int {
        switch self {
        case .cappuccino: return 500
        case .americano: return 0
        case .espresso: return 0
        case .hotMilk: return 1000
        }
    }
    
    var trashNeeded: Int {
        switch self {
        case .cappuccino: return 300
        case .americano: return 500
        case .espresso: return 1000
        case .hotMilk: return 0
        }
    }
}


class CoffeeMachineUI {
    var coffeeTank = 0
    var waterTank = 0
    var milkTank = 0
    var trashTank = 0
    
    let trashMaxTank = 4000
    
    func menu() {
        let line_length = 50
        let menu = "MENU"
        
        print("\n+\(String(repeating: "-", count: 48))+")
        let spacesLR = (line_length - menu.count - 2) / 2
        print("|\(String(repeating: " ", count: spacesLR))\(menu)\(String(repeating: " ", count: spacesLR))|")
        print("+\(String(repeating: "-", count: 48))+")
        
        let all_drinks = [
            DrinkType.americano,
            DrinkType.cappuccino,
            DrinkType.espresso,
            DrinkType.hotMilk
        ]
        
        for drink in all_drinks {
            var alignment = 1
            if drink.rawValue.count % 2 == 0 {
                alignment = 0
            }
            let spacesLR = (line_length - drink.rawValue.count - 2) / 2
            print("|\(String(repeating: ".", count: spacesLR))\(drink)\(String(repeating: ".", count: spacesLR + alignment))|")
        }
        print("+\(String(repeating: "-", count: 48))+\n")
    }
    
    func showTankInfo() -> String{
        return """
            left coffee: \(coffeeTank)\n
            left water: \(waterTank)\n
            left milk: \(milkTank)\n
            trash filled on: \(trashTank)
        """
    }

    func makeADrink(_ drink: DrinkType) -> String {
        var err: String = ""
        var is_success = true
        
        if coffeeTank < drink.coffeeNeeded {
            err = "ERROR - Please add coffee"
            is_success = false
        }
        if waterTank < drink.waterNeeded {
            err = "ERROR - Please add water"
            is_success = false
        }
        if milkTank < drink.milkNeeded {
            err = "ERROR - Please add milk"
            is_success = false
        }
        if trashTank >= trashMaxTank {
            err = "ERROR - Please clear trash tank"
            is_success = false
        }
        
        if is_success {
            trashTank = trashTank + drink.trashNeeded
            coffeeTank = coffeeTank - drink.coffeeNeeded
            waterTank = waterTank - drink.waterNeeded
            milkTank = milkTank - drink.milkNeeded
            
            return "\(drink) is ready!"
        } else {
            return err
        }
    }
    
    func addMilk(xProportion: Int = 1) -> String{
        milkTank += 2000 * xProportion
        
        return "MILK is added"
    }
    func addCoffee(xProportion: Int = 1) -> String{
        coffeeTank += 1000 * xProportion
        
        return "COFFEE is added"
    }
    func addWater(xProportion: Int = 1) -> String{
        waterTank += 4000 * xProportion
        
        return "WATER is added"
    }
    func clearTrash() -> String{
        trashTank = 0
        
        return "TRASH tank is cleared"
    }
}
